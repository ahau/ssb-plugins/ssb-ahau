const gql = require('graphql-tag')
const { promisify: p } = require('util')

const CanEdit = require('../lib/can-edit')
const ALL_AUTHORS = '*'

/*
  having this in ssb-ahau allows a central place (specific to whakapapa-ora) for
  authors to use the same typeDefs across all modules

  TODO: if messy, this should be removed into its own module @ssb-graphql/ahau
*/
module.exports = (ssb, externalGetters) => {
  const { resolvers, gettersWithCache } = Resolvers(ssb, externalGetters)

  return {
    typeDefs,
    resolvers,
    gettersWithCache
  }
}

const typeDefs = gql`
  extend interface Profile {
    authors: [Author]
    canEdit: Boolean
    originalAuthor: String
  }

  extend type Person {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
    originalAuthor: String
  }

  extend type Community {
    authors: [Author]
    kaitiaki: [Kaitiaki]
    canEdit: Boolean
    originalAuthor: String
  }

  extend type Pataka {
    authors: [Author]
    kaitiaki: [Kaitiaki]
    canEdit: Boolean
    originalAuthor: String
  }

  extend type WhakapapaView {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
    originalAuthor: String
  }

  extend type Story {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
    originalAuthor: String
  }

  extend type Collection {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
    originalAuthor: String
  }

  extend type Settings {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
    originalAuthor: String
  }

  extend interface Artefact {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
  }

  extend type Video {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
  }
  extend type Audio {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
  }
  extend type Photo {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
  }
  extend type Document {
    authors: [Author]
    tiaki: [Person]
    canEdit: Boolean
  }

  type Author {
    feedId: ID!
    intervals: [AuthorInterval]
    profile: Person
    active: Boolean
  }

  type Kaitiaki {
    feedId: ID!
    profile: Person
  }
  
  type AuthorInterval {
    start: Float!
    end: Float
  }
  
`

function Resolvers (ssb, externalGetters) {
  const {
    getProfile: _getProfile
  } = externalGetters
  const gettersWithCache = {}

  const getProfile = p(_getProfile)
  const _canEdit = p(CanEdit(ssb))

  const tiaki = async (obj) => {
    const profile = await getProfile(obj.originalAuthor)
    return [profile]
  }

  const loadAuthorsActiveState = async (state) => {
    const { authors, recps: profileRecps } = state

    return Promise.all(
      Object.entries(authors).map(async ([feedId, intervals]) => {
        return {
          feedId,
          profileRecps,
          intervals,
          active: await _canEdit(feedId, { [feedId]: intervals })
        }
      })
    )
  }

  function sharedAuthorResolvers () {
    return {
      authors: loadAuthorsActiveState,
      tiaki,
      canEdit: ({ authors }) => _canEdit(ssb.id, authors)
    }
  }

  async function resolveAuthorProfile ({ feedId, profileRecps }) {
    // for public profile with no recps, we return a public profile
    if (feedId === ALL_AUTHORS) return null
    if (!profileRecps) return getProfile(feedId)

    // determine the groupId from the recps of the community
    // WARNING: assuming there is only one recps!
    const groupId = profileRecps[0]

    // get all profiles associated with this author
    const profiles = await p(ssb.profile.findByFeedId)(feedId, { getProfile })

    // find the profile for this person in the group
    // taking the first profile
    const profile = profiles.private.find(profile => {
      return profile.recps[0] === groupId
    })

    // no private profile so we just use the public one instead
    if (!profile) return getProfile(feedId)

    return profile
  }

  const resolvers = {
    Person: sharedAuthorResolvers(),
    Community: {
      canEdit: ({ authors }) => _canEdit(ssb.id, authors), // current instance is an active author
      authors: loadAuthorsActiveState,
      kaitiaki: async (parent) => {
        const kaitiaki = await loadAuthorsActiveState(parent)

        return kaitiaki
          .filter(k => k.feedId !== '*' && k.active) // discard any * cases or non-active authors
      }
    },
    WhakapapaView: sharedAuthorResolvers(),
    Story: sharedAuthorResolvers(),
    Collection: sharedAuthorResolvers(),
    Settings: sharedAuthorResolvers(),
    Artefact: sharedAuthorResolvers(),
    Video: sharedAuthorResolvers(),
    Audio: sharedAuthorResolvers(),
    Photo: sharedAuthorResolvers(),
    Document: sharedAuthorResolvers(),
    Author: {
      profile: resolveAuthorProfile
    },
    Kaitiaki: {
      profile: resolveAuthorProfile
    }
  }

  return {
    resolvers,
    gettersWithCache
  }
}
