const pull = require('pull-stream')
const { where, type, toPullStream } = require('ssb-db2/operators')

module.exports = {
  up (ssb, misc, cb) {
    findMyTribes((err, tribes) => {
      if (err) return cb(err)

      pull(
        pull.values(tribes),
        pull.asyncMap((tribe, cb) => {
          wasIAddedToGroup(tribe, (err, bool) => {
            if (err) cb(err)
            else cb(null, !bool ? tribe : null)
          })
        }),
        pull.asyncMap((tribe, cb) => {
          if (tribe === null) cb(null)
          else ssb.tribes.invite(tribe, [ssb.id], {}, cb)
        }),
        pull.collect(cb)
      )
    })
    // find all groups that I created
    function findMyTribes (cb) {
      pull(
        ssb.db.query(
          where(type('group/init')),
          toPullStream()
        ),
        pull.filter(msg => msg.value.author === ssb.id),
        pull.map(msg => msg.key),
        pull.collect((err, roots) => {
          if (err) return cb(err)
          ssb.tribes.list((err, groupIds) => {
            if (err) return cb(err)
            pull(
              pull.values(groupIds),
              pull.asyncMap((groupId, cb) => {
                ssb.tribes.get(groupId, (err, groupData) => {
                  if (err) return cb(err)
                  cb(null, { groupId, ...groupData })
                })
              }),
              pull.filter(groupData => roots.includes(groupData.root)),
              pull.map(groupData => groupData.groupId),
              pull.collect(cb)
            )
          })
        })
      )
    }
    // check if i was added to them
    function wasIAddedToGroup (groupId, cb) {
      pull(
        // src
        ssb.db.query(
          where(type('group/add-member')),
          toPullStream()
        ),
        pull.filter(msg => {
          const recps = msg.value.content.recps
          return recps.includes(groupId) && recps.includes(ssb.id)
        }),
        // sink
        pull.collect((err, results) => {
          if (err) cb(err)
          else cb(null, results.length > 0)
        })
      )
    }
    // if not added, add me
  }
}
