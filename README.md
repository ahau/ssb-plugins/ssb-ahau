# ssb-ahau

A module which has a bunch of things for running Ahau desktop app:
- **a graphql server** for the UI to communicate with the backend
    - includes all the `@ssb-graphql/` modules and connects them with federation
    - patches all the relevent types with authors fields

- **an private profile function**
    - when you create or join a private group it creates a copy of your personal groups private profile into that group


## API

### `server.ahau.onReady(cb)`

Provides a way to check if this module and it's internal setup is initialised.
Executes callback with `cb(null, true)` when ready

## Extras

### Generate migration file and test file

`npm run generate:migration <migration-name>`:

- generate a new migration file template in `src/migrations/`
- generate a new test for this migration in `test/migrations/`
- if no arg for `<migration-name>` is given, it defaults to `untitled`
- files will be named: `${Date.now()}-<migration-name>`
- migrations are run before the graphql server starts up
