const test = require('tape')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')

const getProfile = `
query ($id: String!){
  profile(id: $id) {
    id
    preferredName
    recps
    ...on Community {
      kaitiaki {
        feedId
        profile {
          id
          email
          recps
        }
      }
    }
    authors {
      active
      feedId
      intervals {
        start
        end
      }
      profile {
        id
        legalName
        originalAuthor
        recps
      }
    }
  }
}
`

test('resolve community profile kaitiaki and authors', async t => {
  const { ssb: cherese, apollo } = await TestBot()

  // declare methods
  async function runWhoami () {
    const res = await apollo.query({
      query: `
        query {  
          whoami {
            personal {
              profile {
                id
                preferredName
                legalName
              }
            }
          }
        }
      `
    })

    t.error(res.errors, 'run whoami without error')

    return res.data.whoami
  }

  const handleErr = (err) => {
    console.log(JSON.stringify(err, null, 2))
    return { errors: [err] }
  }

  async function savePerson (input) {
    const res = await apollo.mutate({
      mutation: `
        mutation ($input: PersonProfileInput!) {
          savePerson (input: $input)
        }
      `,
      variables: {
        input
      }
    })
      .catch(handleErr)

    t.error(res.errors, 'saves profile/person')

    return res.data.savePerson // profileId
  }

  async function initGroup (input) {
    const res = await apollo.mutate({
      mutation: `
        mutation ($communityProfile: CommunityProfileInput) {
          initGroup(communityProfile: $communityProfile) {
            groupId
          }
        }
      `,
      variables: {
        communityProfile: input
      }
    })
      .catch(handleErr)

    t.error(res.errors, 'init group without error')
    if (res.errors) console.log(JSON.stringify(res.errors, null, 2))

    return res.data.initGroup.groupId
  }

  async function runGetProfile (id) {
    const res = await apollo.query({
      query: getProfile,
      variables: {
        id
      }
    })

    t.error(res.errors, 'get profile without error')

    return res.data.profile
  }

  /**
   * TESTS
   */

  // 1. get personal profile
  let whoami = await runWhoami()

  // 2. check whoami returns correct results
  t.deepEqual(
    whoami,
    {
      personal: {
        profile: {
          id: whoami.personal.profile.id, // hack
          preferredName: null,
          legalName: null
        }
      }
    },
    'returns correct (empty) personal profile'
  )

  // 3. save some fields to our personal profile, so that the profile in the group has them
  const input = {
    preferredName: 'Cherese',
    legalName: 'Cherese Eriepa'
    // email: 'cherese@ahau.io' // TODO re-enable to trigger kaitiaki-only field updates?
  }
  await savePerson({
    id: whoami.personal.profile.id,
    ...input
  })
  console.log('TODO - re-enable saving email to personal profile')

  // 4. check whoami returns correct results
  whoami = await runWhoami()
  t.deepEqual(
    whoami,
    {
      personal: {
        profile: {
          id: whoami.personal.profile.id, // hack
          ...input
        }
      }
    },
    'returns correct (populated) personal profile'
  )

  // 5. create the group
  const communityProfile = {
    preferredName: 'Plant Collectors'
  }

  const groupId = await initGroup(communityProfile)

  // 6. check the group profiles were linked
  const groupProfiles = await p(cherese.profile.findByGroupId)(groupId)

  t.equals(groupProfiles.public.length, 1, 'returns 1 public group profile')
  t.equals(groupProfiles.private.length, 1, 'returns 1 private group profile')

  // 7. check the community profiles were saved and the user was
  // added as a kaitiaki and author
  const communityProfileId = groupProfiles.private[0].key
  const community = await runGetProfile(communityProfileId)
  const feedProfiles = await p(cherese.profile.findByFeedId)(cherese.id)
  const profileId = feedProfiles.private.find(profile => profile.recps[0] === groupId)?.key
  t.deepEqual(
    community,
    {
      id: communityProfileId,
      preferredName: 'Plant Collectors',
      recps: [groupId],
      kaitiaki: [
        {
          feedId: cherese.id,
          profile: {
            id: profileId,
            email: null,
            recps: [groupId]
          }
        }
      ],
      authors: [
        {
          active: true,
          feedId: cherese.id,
          intervals: [{ start: 23, end: null }],
          profile: {
            id: profileId,
            legalName: 'Cherese Eriepa',
            originalAuthor: cherese.id,
            recps: [groupId]
          }
        }
      ]
    },
    'returns kaitiaki + author profile as the one encrypted to the group'
  )

  cherese.close()
  t.end()
})
