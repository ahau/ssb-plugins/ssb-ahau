const test = require('tape')
const TestBot = require('../../test-bot')

function Save (apollo) {
  return async function save (input) {
    return apollo.mutate({
      mutation: `mutation($input: SettingsInput!) {
        saveSettings(input: $input)
      }`,
      variables: { input }
    })
  }
}

function Get (apollo) {
  return async function get (id) {
    return apollo.query({
      query: `query($id: ID!) {
        settings(id: $id) {
          canEdit
          tiaki {
            id
            preferredName
          }
          authors {
            feedId
            intervals {
              start
              end
            }
            profile {
              id
              preferredName
            }
          }
        }
      }`,
      variables: {
        id
      }
    })
  }
}

test('settings - authors', async t => {
  t.plan(13)

  const { ssb, apollo } = await TestBot({ recpsGuard: true })

  const save = Save(apollo)
  const get = Get(apollo)

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          feedId,
          profile {
            id
          }
        }
      }
    }`
  })

  t.error(result.errors, 'query should not return errors')

  const feedId = ssb.id
  const publicProfileId = result.data.whoami.public.profile.id

  const saveSettingsRes = await save(
    {
      authors: {
        add: [feedId],
        remove: []
      },
      recps: [feedId]
    }
  )

  t.error(saveSettingsRes.errors, 'creates settings without errors')
  const settingsId = saveSettingsRes.data.saveSettings

  const settingsRes = await get(settingsId)
  t.error(settingsRes.errors, 'gets settings without errors')

  const settings = settingsRes.data.settings
  t.true(settings.canEdit, 'returns true for canEdit')

  t.deepEqual(
    settings.tiaki,
    [
      {
        id: publicProfileId,
        preferredName: null
      }
    ],
    'returns correct tiaki'
  )

  t.deepEqual(
    settings.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: null } // will it always be the 6th message?
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      }
    ],
    'adds author'
  )

  // ADD * as authors
  const saveRes = await save(
    {
      id: settingsId,
      authors: {
        add: ['*'],
        remove: [feedId] // remove myself
      }
    }
  )

  t.error(saveRes.errors, 'updates settings without errors')

  const updatedSettingsRes = await get(settingsId)
  t.error(updatedSettingsRes.errors, 'gets updated settings without error')

  const _settings = updatedSettingsRes.data.settings
  t.true(_settings.canEdit, 'can still edit because *')

  // HACK: cant test dates?
  const { start } = _settings.authors[1].intervals[0]

  t.deepEqual(
    _settings.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end: null }
        ],
        profile: null
      }
    ],
    'returns intervals with closed state for feedId and active state for *'
  )

  // remove feedId again
  await save(
    {
      id: settingsId,
      authors: {
        remove: [feedId] // the feedId should be ignored because its already removed
      }
    }
  )

  const deletedAuthorRes = await get(settingsId)
  t.error(deletedAuthorRes.errors, 'deletes authors without error')
  const settings2 = deletedAuthorRes.data.settings

  // HACK: cant test dates?
  const { end } = settings2.authors[1].intervals[0]

  t.true(settings2.canEdit, 'can still edit as the original author')

  t.deepEqual(
    settings2.authors,
    [
      {
        feedId,
        intervals: [
          { start: 9, end: 10 }
        ],
        profile: {
          id: publicProfileId,
          preferredName: null
        }
      },
      {
        feedId: '*',
        intervals: [
          { start, end }
        ],
        profile: null
      }
    ],
    'returns the intervals with end values'
  )

  ssb.close()
})
