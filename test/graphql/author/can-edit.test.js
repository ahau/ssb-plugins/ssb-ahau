const test = require('tape')
const TestBot = require('../../test-bot')
const { replicate } = require('scuttle-testbot')

function handleErr (err) {
  console.log(JSON.stringify(err, null, 2))
  return ({ errors: [err], err })
}

function Save (t, apollo) {
  return async function save (input) {
    input.type = 'person'
    const res = await apollo.mutate({
      mutation: `mutation($input: PersonProfileInput!) {
        savePerson(input: $input)
      }`,
      variables: { input }
    })
      .catch(handleErr)

    t.error(res.errors, 'saves profile/person without errors')
    return res.data.savePerson
  }
}

function GetPerson (t, apollo) {
  return async function get (id) {
    const res = await apollo.query({
      query: `query($id: String!) {
        person(id: $id) {
          canEdit
        }
      }`,
      variables: {
        id
      }
    })
      .catch(handleErr)

    t.error(res.errors, 'gets profile without error')
    return res.data.person
  }
}

test('can-edit (me can edit)', async t => {
  const { ssb, apollo } = await TestBot({ recpsGuard: false, graphql: { loadContext: false } })
  const save = Save(t, apollo)
  const getPerson = GetPerson(t, apollo)

  const input = {
    authors: {
      add: [ssb.id] // add self as the author
    }
  }

  const profileId = await save(input)
  const profile = await getPerson(profileId)

  t.deepEqual(
    profile.canEdit,
    true,
    'can edit returns true when im a listed author'
  )

  ssb.close()
  t.end()
})

test('can-edit (friend can edit)', async t => {
  const { ssb: me, apollo: meApollo } = await TestBot({ recpsGuard: false, graphql: { loadContext: false } })
  const { ssb: friend, apollo: friendApollo } = await TestBot({ recpsGuard: false, graphql: { loadContext: false } })

  const save = Save(t, meApollo)
  const meGet = GetPerson(t, meApollo)

  const friendGet = GetPerson(t, friendApollo)

  const input = {
    authors: {
      add: [friend.id] // add friend as the author, not myself
    }
  }

  const profileId = await save(input)
  const profile = await meGet(profileId)

  t.deepEqual(
    profile.canEdit,
    false,
    'original author cannot edit'
  )

  replicate({ from: me, to: friend }, async (err) => {
    t.error(err, 'replicate profile from me to friend')

    // friend gets the record
    const profileByFriend = await friendGet(profileId)

    t.deepEqual(
      profileByFriend.canEdit,
      true,
      'friend can edit because they are listed and active'
    )

    me.close()
    friend.close()
    t.end()
  })
})
