const test = require('tape')
const isActiveAuthor = require('../../../src/lib/is-active-author')

const PASS = 'pass'
const FAIL = 'fail'
const THROWS = 'throws'

function Interval (start, end) {
  return { start, end }
}

test('isActiveAuthor', t => {
  function runTest (intervals, seq, passes, msg) {
    switch (passes) {
      case PASS:
        t.true(isActiveAuthor(intervals, seq), 'isActiveAuthor - ' + msg)
        return
      case FAIL:
        t.false(isActiveAuthor(intervals, seq), 'isActiveAuthor - ' + msg)
        return
      case THROWS:
        t.throws(() => isActiveAuthor(intervals, seq), 'isActiveAuthor - ' + msg)
        return
      default:
        throw new Error('something went wrong with isActiveAuthor.runTest')
    }
  }

  //
  // TRUE
  //

  runTest(
    [Interval(1, null)],
    2,
    PASS,
    'returns true when it contains an open interval'
  )

  runTest(
    [
      Interval(1, 100),
      Interval(110, 300),
      Interval(370, null)
    ],
    500,
    PASS,
    'returns true when it contains an open interval'
  )

  //
  // FALSE
  //

  runTest(
    [Interval(undefined, undefined)],
    1,
    FAIL,
    'returns null when start and end is undefined'
  )

  runTest(
    [Interval(null, null)],
    1,
    FAIL,
    'returns false when start is null and end is null'
  )

  runTest(
    [Interval(undefined, 1)],
    1,
    FAIL,
    'returns false when start is undefined'
  )

  runTest(
    [Interval(null, 1)],
    1,
    FAIL,
    'returns false when start is null'
  )

  runTest(
    [Interval(1, null)],
    1,
    PASS, // passes start === seq
    'returns true when start === seq???'
  )

  runTest(
    [Interval(2, null)],
    1,
    FAIL,
    'returns false when the interval is closed (end isnt null)'
  )

  runTest(
    [Interval(1, 5)],
    2,
    PASS,
    'returns true when the sequence falls within the interval'
  )

  runTest(
    [Interval(1, undefined)],
    2,
    FAIL,
    'returns false as the end needs to be null for it to be open'
  )

  runTest(null, null, FAIL, 'returns false when intervals is null')
  runTest(undefined, undefined, FAIL, 'returns false when intervals is undefined')
  runTest({}, 1, THROWS, 'throws error when intervals is an object')
  runTest(true, 1, THROWS, 'throws error when intervals is a true boolean')
  runTest(false, 1, THROWS, 'throws error when intervals is a false boolean')

  t.end()
})
