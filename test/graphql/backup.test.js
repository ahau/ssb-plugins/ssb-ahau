const test = require('tape')
const Server = require('../test-bot')

test('get backup', async t => {
  const { apollo, ssb } = await Server({ loadContext: true })
  const res = await apollo.query({
    query: `query {
      backup
    }
    `
  })

  const { backup } = res.data
  t.error(res.errors, 'Gets backup without errors')

  const jsonBackup = JSON.parse(backup.split('\n').filter(line => line.indexOf('#') !== 0).join(''))

  t.deepEqual(jsonBackup.secret, ssb.keys, 'Gets the correct secret')

  t.deepEqual(jsonBackup.ownKeys.length, 1, 'One ownKey')
  const ownKey = jsonBackup.ownKeys[0]
  t.deepEqual(typeof ownKey.key, 'string', 'ownKey.key')
  t.deepEqual(Buffer.from(ownKey.key, 'base64').length, 32, 'ownKey.key')
  t.deepEqual(ownKey.scheme, 'envelope-symmetric-key-for-self', 'ownKey.scheme')

  t.equal(jsonBackup.latestSequence, 9, 'Gets correct sequence number')

  ssb.close()
  t.end()
})
