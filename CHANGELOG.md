# Changelog | ssb-ahau

## v12.1.0

- add a new query to get the indexing status of the ssb server
- rename ssb everywhere

## v12.0.2

- update outdated dependencies
- bug fix for upgraded profiles: add support for profile type pataka + authors

## v12.0.0

- upgrade dependencies to use new upgraded profiles
- move `auto-prune` to `ssb-hyper-blobs`
- ensure unique ports on test-bot

## v11.3.3

- resolve Community Kaitiaki and Author profiles to their private one in the group (or public one)

## v11.3.2

- Update `@ssb-graphql/tribes` dependency with profileId on applications

## v11.3.1

- Fix bug where auto private profile was trying to save profile with deprecated fields

## v11.3.0

- update `@ssb-graphql/tribes`
  - applications now take a profileId to resolve the applicants profile
- update `@ssb-graphql/profile`
  - new private profile/person spec weaved into existing `saveProfile` mutation and `profile` query
  - bug fix where a public profile that was tombstoned was calling back with an error in the `profile` query

## v11.2.0

- add a graphql mutation to delete the ahau folder

## v11.1.0

- refactor of graphql modules
- added "mount" to `fileSystemStats` endpoint

## v11.0.0

- adds kaitiaki field to community typeDef
  - resolves active authors as kaitiaki
  - removed tiaki field from Profile and Community typeDefs still available on the Person type
- update `@ssb-graphql/profile` which allows tombstoning of public profiles

## v10.2.4

- fixed cors issue with undefined origin for cordova

## v10.2.3

- auto prune private info from public profiles on startup

## v10.2.2

- fix bug where graphiql playground couldnt be accessed because of cors

## v10.2.1

- adds CORS config to protect our GraphQL API from:
  - others on our network
  - scripts running in websites we load
- add query `backup`: gets all the things you need save in case you lose your device and need to build your identity back up

- refactored test-bot to use the same setup as production. allowed us to spot some faulty tests, and use simpler dependancies

- add a config option to auto-prune hyperBlobs when the total size of ahau is greater than 5GB

## v10.1.1

- refactor

## v10.1.0

- added query `latestSequence`

## v10.0.0

- Updates ssb-graphql-main which has a fix for file uploads
- Replaces `apollo-server` with `express-graphql`
